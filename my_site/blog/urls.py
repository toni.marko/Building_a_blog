#ch5, k 57, kreirali flile za pocetak, plan u ovom koraku je dodavati url-ove i view-ove

from django.urls import path

from . import views

urlpatterns = [    #dodajemo patheve koje ce django traziti, pathevi su dodani prema slajdu o planiranju projekta (korak 56),
                    # nakon pathova idemo dodati viewove i views.py

    path("", views.starting_page, name="starting-page"),  #ubiti nekakav homepage, views.starting_page kao i name dio dodan naknadno nakon kreiranja funkcija u views.py
    path("posts", views.posts, name="posts-page"),   #prvi dio, views.post dio kao i name dio dodan naknadno nakon kreiranja funkcije
    path("posts/<slug:slug>", views.post_detail, name="post-detail-page")  #dinamicki dio   #/posts/my-first-post   -> ovaj my-first-post dio se zove slug, zato je tako nazvan dinamicki dio
                                #slug: je ugradeni path transformer za slugove (slicno kao str i int sto smo koristili vec)
                                #on provjerava da vrijednost koja se predaje u slug ima format slug, tj da 
                                #je to tekst koji sadrzi slova i brojeve i moze sadrzavati - (dashes), ne smije druge specijalne znakove. ugl ovakav (/my-first-post) format
                                # ako url ne izgleda tako, path se nece triggerati
]  #uz sve to ove url-ove idemo sada povezati u urls.py file od my_page kako bi ih projekt znao
