from django.shortcuts import render
from datetime import date

  #ch5. k64 dodali smo dummy data koji ce se dinamicku ucitavati na templateove, kasnije ce se dodati cijeli datbase
all_posts = [
    {
        "slug": "hike-in-the-mountains",
        "image": "mountains.jpg",
        "author": "Maximilian",
        "date": date(2021, 7, 21),
        "title": "Mountain Hiking",
        "excerpt": "There's nothing like the views you get when hiking in the mountains! And I wasn't even prepared for what happened whilst I was enjoying the view!",
        "content": """
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.

          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.

          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.
        """
    },
    {
        "slug": "programming-is-fun",
        "image": "coding.jpg",
        "author": "Maximilian",
        "date": date(2022, 3, 10),
        "title": "Programming Is Great!",
        "excerpt": "Did you ever spend hours searching that one error in your code? Yep - that's what happened to me yesterday...",
        "content": """
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.

          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.

          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.
        """
    },
    {
        "slug": "into-the-woods",
        "image": "woods.jpg",
        "author": "Maximilian",
        "date": date(2020, 8, 5),
        "title": "Nature At Its Best",
        "excerpt": "Nature is amazing! The amount of inspiration I get when walking in nature is incredible!",
        "content": """
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.

          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.

          Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis nobis
          aperiam est praesentium, quos iste consequuntur omnis exercitationem quam
          velit labore vero culpa ad mollitia? Quis architecto ipsam nemo. Odio.
        """
    }
]

def get_date(post):
    return post['date']  #ch5.k65 sortiramo postove po datumu

# Create your views here.

#prvo kreiramo pocetne funkcije, samo ih definiramo, i odemo u urls.py da ih povezemo sa pathom

def starting_page(request): #kreiramo prvu funkciju. tice se pocetne stranice
    sorted_posts = sorted(all_posts, key=get_date)
    latest_posts = sorted_posts[-3:]   #ch5. k65. trazimo tri najnovija posta
    return render(request, "blog/index.html", {  #ch5, k58 renderamo kako bi se ucitao pocetni site prilikom posjete url-u tom
        "posts" : latest_posts  #zadnje postove renderamo na pocetnoj stranici
    })
    

def posts(request):    #kreiramo  funkciju. tice se postova
    return render(request, "blog/all-posts.html", {
        "all_posts": all_posts  #ch5. k66.dodajem listu skroz gore kao vrijednost keyu "all_posts", to dodaje all_posts value u all-posts template
    })
          
    

def post_detail(request, slug): #kreiramo  funkciju. tice se zasebnih (individualnih) postova
    identified_post = next(post for post in all_posts if post['slug'] == slug) #ch5. k66 prolazi kroz listu rijecnika all_posts i trazi podudara li se slug 
                                                                                # koji prima kao argument sa nekim u rijecniku u toj listi
                                                                                # ako se podudara, pridruzi ga kao vrijednost "post" value keyu
    return render(request, "blog/post-detail.html", {
        "post": identified_post
    })